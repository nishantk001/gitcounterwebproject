package com.qaagility.controller;

public class Counter {

    public int denom(int num1, int num2) {
        if (num2 == 0) {
            return (Integer.MAX_VALUE);
          }
        else {
            return (num1 / num2);
          }
    }

}
